// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import 'mdbvue/build/css/mdb.css';
import Vue from 'vue';
import App from '../src/App';
import Login from '../src/components/Login';
import SignUp from '../src/components/SignUp';
import Home from '../src/components/Home';
import Dashboard from '../src/components/Dashboard';
import CreateProject from '../src/components/CreateProject';
import CreateIssue from '../src/components/CreateIssue';
import ViewProject from '../src/components/ViewProject';
import ViewIssue from '../src/components/ViewIssue';
import ViewIssuesAssigned from '../src/components/ViewIssuesAssigned';
import ViewIssuesReported from '../src/components/ViewIssuesReported';
import SearchProjects from '../src/components/SearchProjects';
import SearchIssues from '../src/components/SearchIssues';

import VueSession from 'vue-session';

import Router from 'vue-router';

import Axios from "axios";

Vue.use(VueSession);

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token');
if (token) {
  Axios.defaults.headers.common.Authorization = token ;
}

Vue.use(Router);

const routes = new Router({
  mode: 'history',
  routes: [{
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/',
    redirect: {
      name: "Login",
      component: Login
    }
  },

  {
    path: '/logout',
    redirect: {
      name: "Login",
      component: Login
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    props : true
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    props : true
  },
  {
    path: '/viewIssuesAssigned',
    name: 'ViewIssuesAssigned',
    component: ViewIssuesAssigned,
    props : true
  },
  {
    path: '/viewIssuesReported',
    name: 'ViewIssuesReported',
    component: ViewIssuesReported,
    props : true
  },
  {
    path: '/createProject',
    name: 'CreateProject',
    component: CreateProject,
    props: true
  },
  {
    path: '/createIssue',
    name: 'CreateIssue',
    component: CreateIssue,
    props: true
  },
  {
    path: '/viewProject',
    name: 'ViewProject',
    component: ViewProject,
    props: true
  },
  {
    path: '/viewIssue',
    name: 'ViewIssue',
    component: ViewIssue,
    props: true
  },
  {
    path: '/searchProjects',
    name: 'SearchProjects',
    component: SearchProjects,
    props: true
  },
  {
    path: '/searchIssues',
    name: 'SearchIssues',
    component: SearchIssues,
    props: true
  }
  ]
});

export default routes; 
/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router: routes
  //components: { App },
  //template: '<App/>'
});
