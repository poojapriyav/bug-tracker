{
    "name": "order-service",
    "server": {
        "instances": [
            {
                "label": "public",
                "port": "{{server.public_port}}"
            },
            {
                "label": "private",
                "port": "{{server.private_port}}"
            }
        ],
        "logging_file_path": "{{server.log_path}}",
        "worker": {
            "messaging-worker": {
                "http": {
                    "port": "{{server.workers_port.messaging_worker}}"
                }
            }
        }
    },
    "app_config":{
        "GUEST_CANCELLATION_TTL_MINUTES": "{{service.guest_cancellation_ttl_minutes}}",
        "QUEUE_DELAY_TIME_IN_SEC": "{{service.queue_delay_time_in_sec}}",
        "REQUEUE_DELAY_TIME": "{{service.requeue_delay_time}}",
        "cache": {
            "cachingDisabled": "{{service.cache.caching_disabled}}",
            "db": "{{service.cache.db}}",
            "redisDataDefaultTtlInSecs": "{{service.cache.redis_data_default_ttl_in_secs}}",
            "in_memory": {
              "epp_store": {
                "enabled": "{{service.cache.in_memory.epp_store.enabled}}",
                "ttl": "{{service.cache.in_memory.epp_store.ttl}}"
              }
            }
        },
        "channel": "{{service.channel}}",
        "consent_sync_delay": "{{service.consent_sync_delay}}",
        "elasticsearch": {
            "host": "{{infra.data_elastic_search.host}}",
            "port": "{{infra.data_elastic_search.port}}"   
        },
        "exchange_service_rou_auth_secret": "{{infra.exchange_service_rou_auth_secret}}",
        "returns_window": "{{service.returns_window}}",
        "replacement_window": "{{service.replacement_window}}",
        "self_service_replacement_channels": "{{service.self_service_replacement_channels}}",
        "pre_order_release_dates": "{{service.pre_order_release_dates}}",
        "carrier_pre_order_release_before": "{{service.carrier_pre_order_release_before}}",
        "channel_ids": "{{service.channel_ids}}",
        "upgrade_window_period": {
          "unit": "{{service.upgrade_window_period.unit}}",
          "value": "{{service.upgrade_window_period.value}}"
        },
        "channel_store_addresses": "{{service.channel_store_addresses}}",
        "features": "{{service.features}}",
        "fetch_email_from": "{{service.fetch_email_from}}",
        "fraud": {
            "auto_release": {
                "enabled": "{{service.fraud.auto_release.enabled}}",
                "release_time": "{{service.fraud.auto_release.release_time}}",
                "strategy": "{{service.fraud.auto_release.strategy}}"
            },
            "backlog_processing_limit": "{{service.fraud.backlog_processing_limit}}",
            "business_hours": {
                "end": "{{service.fraud.business_hours.end}}",
                "start": "{{service.fraud.business_hours.start}}",
                "timezone": "{{service.fraud.business_hours.timezone}}"
            },
            "elasticsearch": {
                "host": "{{infra.data_elastic_search.host}}",
                "index": "{{service.fraud.elastic_search.index}}",
                "port": "{{infra.data_elastic_search.port}}",
                "type": "{{service.fraud.elastic_search.type}}"
            },
            "minimum_match_threshold": "{{service.fraud.minimum_match_threshold}}",
            "result_limit": "{{service.fraud.result_limit}}"
        },
        "insurance_config": "{{infra.insurance_config}}",
        "fulfiller": "{{partner}}",
        "tenant_id_two_letter": "{{tenant_id_two_letter}}",
        "delivery_partner": "{{delivery_partner}}",
        "keysAuthSecret": "{{infra.keys_auth_secret}}",
        "keys_auth_secret": "{{infra.keys_auth_secret}}",
        "notification": {
            "default_grouping_time": "{{service.notification.default_grouping_time}}",
            "grouping_delay_time": {
                "cancellation_failed": "{{service.notification.grouping_delay_time.cancellation_failed}}",
                "cancellation_succeeded": "{{service.notification.grouping_delay_time.cancellation_succeeded}}",
                "line_item_shipped": "{{service.notification.grouping_delay_time.line_item_shipped}}"
            }
        },
        "notificationServiceAuthSecret": "{{infra.notification_service_auth_secret}}",
        "order_status_callback_url": "{{service.order_status_callback_url}}",
        "payment_service_appid": "{{service.payment_service_app_id}}",
        "payment_service_rou_auth_secret": "{{infra.payment_service_rou_auth_secret}}",
        "payment_sync_delay": "{{service.payment_sync_delay}}",
        "payment_update_allowed_window": "{{service.payment_update_allowed_window}}",
        "po_submission_remorse_period": "{{service.po_submission_remorse_period}}",
        "postgres": {
            "connection_string": "{{infra.postgres.connection_string}}",
            "pool_size": "{{infra.postgres.pool_size}}"
        },
        "pricing_engine_auth_secret": "{{infra.pricing_engine_auth_secret}}",
        "rabbitmq": {
            "heartbeat": "{{infra.rabbitmq.heartbeat}}",
            "host": "{{infra.rabbitmq.host}}",
            "login": "{{infra.rabbitmq.login}}",
            "messaging": {
                "bindings": {
                    "CancelOrderPaymentFailure": [
                        "orders.cancel_order_payment_failure_queue"
                    ],
                    "CancellationFeeds": [
                        "orders.cancellation_feeds_queue"
                    ],
                    "ConsentSync": [
                        "orders.consent_sync_queue"
                    ],
                    "ExchangeSync": [
                        "orders.exchange_sync_queue"
                    ],
                    "ExternalOrderRefundSettlement": [
                        "orders.external_order_refund_settlement_queue"
                    ],
                    "FraudFilters": [
                        "orders.new_fraud_filter_queue"
                    ],
                    "FulfillmentEvents": [
                        "orders.fulfillment_events_queue"
                    ],
                    "InternalSubmitCart": [
                        "orders.internal_submit_cart_queue"
                    ],
                    "NotificationEvents": [
                        "orders.notification_events.events_queue"
                    ],
                    "OrderCoupons": [
                        "orders.coupons_queue"
                    ],
                    "OrderEPP": [
                        "orders.epp_queue"
                    ],
                    "OrderOffers": [
                        "orders.offers_queue"
                    ],
                    "PaymentFailure": [
                        "orders.payment_failure_queue"
                    ],
                    "PaymentSync": [
                        "orders.payment_sync_queue"
                    ],
                    "ShipmentNotifications": [
                        "orders.shipment_notification_queue"
                    ],
                    "SubmitPO": [
                        "orders.submit_po_queue"
                    ],
                    "TrackingOutput": [
                        "orders.{{lower_tenant_id}}-aftership-track-output"
                    ],
                    "Attachments": [
                      "dps.attachments_queue"
                    ],
                    "FiPosting": [
                      "orders.fi_posting_queue"
                    ],
                    "InsuranceEvents": [
                      "orders.insurance_events_queue"
                    ],
                    "InsuranceEventsFeed": [
                      "orders.insurance_events_feed_queue"
                    ],
                    "PaymentCapture": [
                      "orders.payment_capture_queue"
                    ],
                    "PaymentRefund": [
                      "orders.payment_refund_queue"
                    ],
                    "RefundSync": [
                      "orders.refund_sync_queue"
                    ],
                    "FraudCheck":[
                      "orders.fraud_check_queue"
                    ],
                    "DelayNotifications":[
                      "orders.delay_notifications_queue"
                    ],
                    "CarrierSubmit": [
                      "orders.carrier_submit_queue"
                    ],
                    "SyncDeviceInfoToCarrier": [
                      "orders.sync_device_info_to_carrier_queue"
                    ],
                    "ResendReturnLabel": [
                        "orders.resend_return_label_queue"
                    ]
                },
                "exchanges": {
                    "CancelOrderPaymentFailure": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.cancel_order_payment_failure"
                    },
                    "CancellationFeeds": {
                        "bindQueue": "false",
                        "name": "{{lower_tenant_id}}.dps.bulk_cancellations"
                    },
                    "CatalogSync": {
                        "bindQueue": "false",
                        "name": "{{lower_tenant_id}}.catalog.sku_sync"
                    },
                    "ConsentSync": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.consent_sync"
                    },
                    "ExchangeSync": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.exchange_sync"
                    },
                    "FraudFilters": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.fraud_filters"
                    },
                    "FulfillmentEvents": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.fulfillment_events"
                    },
                    "NotificationEvents": {
                        "bindQueue": "false",
                        "name": "{{lower_tenant_id}}.orders.notification_events"
                    },
                    "OrderCoupons": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.coupons"
                    },
                    "OrderEPP": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.epp"
                    },
                    "OrderOffers": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.offers"
                    },
                    "PaymentFailure": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.payment_failure"
                    },
                    "PaymentSync": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.payment_sync"
                    },
                    "RefundSync": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.refund_sync"
                    },
                    "ShipmentNotifications": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.shipment_notification"
                    },
                    "ShipmentTrackingFeeds": {
                        "bindQueue": "false",
                        "name": "{{lower_tenant_id}}.tracking_info"
                    },
                    "SubmitPO": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.orders.submit_po"
                    },
                    "TrackingInput": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.dps.shipment_tracking_input"
                    },
                    "Attachments": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.dps.attachments"
                    },
                    "FiPosting": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.fi_posting"
                    },
                    "FraudCheck": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.fraud_check"
                    },
                    "InsuranceEvents": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.insurance_events"
                    },
                    "InsuranceEventsFeed": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.insurance_events_feed"
                    },
                    "PaymentCapture": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.payment_capture"
                    },
                    "PaymentRefund": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.payment_refund"
                    },
                    "DelayNotifications": {
                      "bindQueue": "true",
                      "name": "{{lower_tenant_id}}.orders.delay_notifications"
                    },
                    "CarrierSubmit": {
                      "bindQueue": "false",
                      "name": "{{lower_tenant_id}}.orders.carrier_submit"
                    },
                    "SyncDeviceInfoToCarrier": {
                      "bindQueue": "false",
                      "name": "{{lower_tenant_id}}.orders.sync_device_info_to_carrier"
                    },
                    "ResendReturnLabel": {
                        "bindQueue": "false",
                        "name": "{{lower_tenant_id}}.orders.resend_return_label"
                    },
                    "ReturnLabel": {
                        "bindQueue": "true",
                        "name": "{{lower_tenant_id}}.dps.return_label"
                    }
                },
                "prefetchCount": "{{infra.rabbitmq.messaging.prefetch_count}}",
                "prefix": "{{infra.rabbitmq.messaging.prefix}}",
                "priorityInstances": {
                    "host1": {
                        "exchanges": {
                            "DRSubmitCart": {
                                "bindQueue": "{{infra.rabbitmq.priority_instances.host1.exchanges.dr_submit_cart.bind_queue}}",
                                "name": "{{infra.rabbitmq.priority_instances.host1.exchanges.dr_submit_cart.name}}"
                            }
                        }
                    },
                    "host2": {
                        "exchanges": {
                            "DRSubmitCart": {
                                "bindQueue": "{{infra.rabbitmq.priority_instances.host2.exchanges.dr_submit_cart.bind_queue}}",
                                "name": "{{infra.rabbitmq.priority_instances.host2.exchanges.dr_submit_cart.name}}"
                            }
                        }
                    }
                }
            },
            "password": "{{infra.rabbitmq.password}}",
            "port": "{{infra.rabbitmq.port}}",
            "protocol": "{{infra.rabbitmq.protocol}}"
        },
        "rateLimiterOptions": {
            "orders_cancel": {
                "duration": "{{service.rate_limiter_options.orders_cancel.duration}}",
                "limit": "{{service.rate_limiter_options.orders_cancel.limit}}"
            },
            "orders_guest_search": {
                "duration": "{{service.rate_limiter_options.orders_guest_search.duration}}",
                "limit": "{{service.rate_limiter_options.orders_guest_search.limit}}"
            },
            "orders_notification_info": {
                "duration": "{{service.rate_limiter_options.orders_notification_info.duration}}",
                "limit": "{{service.rate_limiter_options.orders_notification_info.limit}}"
            },
            "orders_payment": {
                "duration": "{{service.rate_limiter_options.orders_payment.duration}}",
                "limit": "{{service.rate_limiter_options.orders_payment.limit}}"
            },
            "orders_submit": {
                "duration": "{{service.rate_limiter_options.orders_submit.duration}}",
                "limit": "{{service.rate_limiter_options.orders_submit.limit}}"
            },
            "replace_order" : {
                "duration" : "{{service.rate_limiter_options.replace_order.duration}}",
                "limit" : "{{service.rate_limiter_options.replace_order.limit}}"
            }
        },
        "redis": {
            "db": "{{infra.redis.db}}",
            "host": "{{infra.redis.host}}",
            "maxReconnectWaitTime": "{{infra.redis.max_reconnect_wait_time}}",
            "port": "{{infra.redis.port}}",
            "read": {
                "host": "{{infra.redis.read.host}}",
                "port": "{{infra.redis.read.port}}"
            },
            "totalRetryTimeInSeconds": "{{infra.redis.total_retry_time_in_seconds}}",
            "write": {
                "host": "{{infra.redis.write.host}}",
                "port": "{{infra.redis.write.port}}"
            }
        },
        "gerp_common_division": "{{service.gerp_common_division}}",
        "requestTimeout": "{{service.request_timeout}}",
        "samsung_base_url": "{{service.samsung_base_url}}",
        "samsung_invoice_base_url": "{{service.samsung_invoice_base_url}}",
        "invoice_prefix": "{{service.invoice_prefix}}",
        "samsung_my_orders_url": "{{service.samsung_my_orders_url}}",
        "samsung_order_tracking_url": "{{service.samsung_order_tracking_url}}",
        "samsung_orders_lookup_url": "{{service.samsung_orders_lookup_url}}",
        "tenant_id": "{{tenant_id}}",
        "default_currency": "{{tenant.default_currency}}",
        "default_locale":"{{tenant.default_locale}}",
        "service_nginx_base_url":"{{tenant.service_nginx_base_url}}",
        "attachment_type_s3_folders": "{{service.attachment_type_s3_folders}}",
        "vat_percentage": "{{service.vat_percentage}}",
        "s3": "{{infra.s3}}",
        "es_attachments_index": "{{service.es_attachments_index}}",
        "es_attachments_type": "{{service.es_attachments_type}}",
        "static_attachment_docs": "{{service.static_attachment_docs}}",
        "upgrade_eligibility_start_date": {
            "time_span": "{{service.upgrade_eligibility_start_date.time_span}}",
            "time_unit_type": "{{service.upgrade_eligibility_start_date.time_unit_type}}"
        }
    },
    "tenant_id": "{{tenant_id}}"
}