'use strict'
let BaseAppLoader = require('fabric-lib').BaseAppLoader,
  ConfigSchema = require('./lib/schema/database/config-schema'),
  { db } = require('base-lib').pgp,
  repoInfo = require('./repo-info'),
  SQLFileCache = require('./lib/sql'),
  EComInternalCredFilter = require('fabric-lib').Hapi.EComInternalCredFilter;

class AppLoader extends BaseAppLoader {
  constructor() {
    super(repoInfo);
    this.configSchema = ConfigSchema.ServiceConfigSchema;
  }

  fetchBaseRoutes() {
    this.applicationData.publicRoutePrefix = `/v1/${this.repoInfo.name}`;
    this.applicationData.privateRoutePrefix = `/v1/_${this.repoInfo.name}`;
  }

  getSpecificPlugins(instanceConfig, config, dependencies) {
    return [
      {
        plugin: EComInternalCredFilter,
        options: { dependencies: dependencies, config: config }
      }
    ];
  }

  /*registerSpecificStrategies(server, dependencies, config) {
    server.auth.strategy('ecom-internal-cred', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config
    });
    server.auth.strategy('ecom-internal-cred-auth', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config,
      authenticated: true
    });
    server.auth.strategy('ecom-internal-auth-guest', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config,
      authenticated: false
    });
  }*/

  updateConfigAndDependencies() {
    console.log(this.applicationData);
    let { dependencies, config } = this.applicationData;
    dependencies.pgp = db(config.app_config.postgres.connection_string, config.app_config.postgres.pool_size);
    dependencies.sql = SQLFileCache;
  }
}

module.exports = AppLoader;
