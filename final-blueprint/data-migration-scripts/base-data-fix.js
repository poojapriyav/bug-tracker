let nconf = require('nconf'),
  BaseHelper = require('base-lib').BaseHelper,
  ConfigAccessorV2 = require('base-lib').ConfigAccessorV2,
  loggerThread = require('base-lib').LoggerThread,
  ConfigSchema = require('../lib/schema/database/config-schema'),
  joi = require('joi');

class BaseDataFix extends BaseHelper {
  constructor() {
    super();
    this.dependencies = {};
    this.config = {};
    this.repoConfig = {};
  }

  async init() {
    await this._setConfig();
    this._setDependencies();
  }

  async _setConfig() {
    let bootLogPath = `/tmp/${this.applicationData.tenantId}/`
    if (nconf.get('bootLogPath')) {
      bootLogPath = nconf.get('bootLogPath');
    }

    let configAccessor = new ConfigAccessorV2(
      {
        logger: loggerThread(`${this.applicationData.tenantId}-${this.repoInfo.name}-datafix-boot`, bootLogPath)
      },
      {
        repoName: this.repoInfo.name,
        bootConfig: this.bootConfig,
        tenantId: this.applicationData.tenantId
      }
    );

    this.applicationData.config = await configAccessor.init();

    let validationResult = joi.validate(this.applicationData.config, ConfigSchema.ServiceConfigSchema, { abortEarly: false, allowUnknown: true });
    if (validationResult.error) {
      configAccessor.logger.log(`Config error:\n ${validationResult.error}`);
      configAccessor.logger.log('BaseDataFix _setConfig', validationResult.error);
      process.exit(1);
    }
  }

  _setDependencies() {
    //Initialize any data-accessors here.
  }
}

module.exports = BaseDataFix;