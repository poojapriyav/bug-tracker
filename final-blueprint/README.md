**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).duction with minification
npm run build / yarn build

# build for production and view the bundle analyzer report
npm run build --report / yarn build --report
```

# Supported browsers

MDBootstrap supports the **latest, stable releases** of all major browsers and platforms.

Alternative browsers which use the latest version of WebKit, Blink, or Gecko, whether directly or via the platform’s web view API, are not explicitly supported. However, MDBootstrap should (in most cases) display and function correctly in these browsers as well.

### Mobile devices

Generally speaking, MDBootstrap supports the latest versions of each major platform’s default browsers. Note that proxy browsers (such as Opera Mini, Opera Mobile’s Turbo mode, UC Browser Mini, Amazon Silk) are not supported.

|  | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome  | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox  |  [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari   | Android Browser & WebView  |                  [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br> Miscrosoft Edge                      |
|:--------------------:|:---------------------------:|:----------------------------:|:----------------------------:|:----------------------------:|:-------------------------------------------------------------------------:|
| Android | Supported | Supported | N/A | Android v5.0+ supported | Supported |
| iOS | Supported | Supported | Supported | N/A | Supported |
| Windows 10 Mobile | N/A | N/A | N/A | N/A | Supported |

### Desktop browsers

Similarly, the latest versions of most desktop browsers are supported.

|  | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome  |  [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox  | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br> Internet Explorer  |  [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="Internet Explorer / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br> Edge  | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Opera" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Opera                  |       [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari       |
|:--------------------:|:-----------------------------:|:------------------------------:|:------------------------------:|:----------------------------:|:-------------------------------------------------------------------------:|:------------------------------:|
| Mac | Supported | Supported | N/A | N/A | Supported | Supported |
| Windows  | Supported | Supported | N/A | Supported | Supported | Not supported |

# Contributing

Please read [CONTRIBUTING.md](https://github.com/mdbootstrap/Vue-Bootstrap-with-Material-Design/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

# Getting started

 - [5min Quick Start](https://mdbootstrap.com/docs/vue/getting-started/quick-start/)

# Additional tutorials

 - [MDB - Bootstrap tutorial](https://mdbootstrap.com/education/bootstrap/)

 - [MDB - Wordpress tutorial](https://mdbootstrap.com/education/wordpress/)

# PRO version

 - [Vue Bootstrap with Material Design PRO](https://mdbootstrap.com/products/vue-ui-kit/)

# Documentation

[Huge, detailed documentation avilable online](https://mdbootstrap.com/docs/vue/)

# Highlights:

**Bootstrap 4**
Up-to-date with the latest standards of Bootstrap 4 and all the best it has to offer.

**Detailed documentation**
Intuitive and user-friendly documentation, created with a copy-paste approach.

**No jQuery**
Writing you code with pure React is now quicker, easier, and cleaner.

**Cross-browser compatibility**
Works perfectly with Chrome, Firefox, Safari, Opera and Microsoft Edge.

**Frequent updates**
Expect any bugs being fixed in a matter of days.

**Active community**
MDB is broadly used by professionals on multiple levels, who are ready to aid you.

**Useful helpers**
Reduce the frequency of highly repetitive declarations in your CSS.

**Technical support**
Every day we help our users with their issues and problems.

**SASS files**
Thought-out .scss files come in a compile-ready form.

**Flexbox**
Full support of Flexbox layout system lets you forget about alignment issues.

### Support MDB developers

- Star our GitHub repo
- Create pull requests, submit bugs, suggest new features or documentation updates
- Follow us on [Twitter](https://twitter.com/mdbootstrap)
- Like our page on [Facebook](https://www.facebook.com/mdbootstrap)

A big ❤️ **thank you to all our users** ❤️ who are working with us to improve the software. We wouldn't be where we are without you.

# Useful Links

 - [Getting started](https://mdbootstrap.com/docs/vue/getting-started/download/)

 - [5 min quick start](https://mdbootstrap.com/docs/vue/getting-started/quick-start/)

 - [Material Design + Bootstrap Tutorial](https://mdbootstrap.com/education/bootstrap/)

 - [Freebies](https://mdbootstrap.com/freebies/)

 - [Premium Templates](https://mdbootstrap.com/templates/)

 - [Changelog](https://mdbootstrap.com/vue/changelog/)
 
 - [MDB jQuery repo](https://github.com/mdbootstrap/bootstrap-material-design)

 - [MDB React repo](https://github.com/mdbootstrap/React-Bootstrap-with-Material-Design)

 - [MDB Angular repo](https://github.com/mdbootstrap/Angular-Bootstrap-with-Material-Design)

# Social Media

 - [Twitter](https://twitter.com/MDBootstrap)

 - [Facebook](https://www.facebook.com/mdbootstrap) 

 - [Pinterest](https://pl.pinterest.com/mdbootstrap)

 - [Google+](https://plus.google.com/u/0/b/107863090883699620484/+Mdbootstrap/posts)

 - [Dribbble](https://dribbble.com/mdbootstrap)

 - [LinkedIn](https://www.linkedin.com/company/material-design-for-bootstrap)

# Contact

 - contact@mdbootstrap.com
