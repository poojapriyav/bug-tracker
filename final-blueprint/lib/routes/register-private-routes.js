'use strict';

const repoInfo = require('../../repo-info'),
  BlueprintRouteHandler = require('./blueprint-route-handler');


class RegisterPrivateRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.blueprintRouteHandler = new BlueprintRouteHandler(dependencies, this.config);
    this.config.appId = repoInfo.name;
  }

  async init(server){
    server.log('Init done for Private Routes')
  }

  registerRoutes(server) {
    server.log('No private routes to register for Blueprint service');
  }
}

module.exports = RegisterPrivateRoutes;
