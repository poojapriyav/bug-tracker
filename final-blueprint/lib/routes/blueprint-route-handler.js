'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  repoInfo = require('../../repo-info'),
  BlueprintService = require('../service/blueprint-service'),
  UserService = require('../service/user-service'),
  ProjectService = require('../service/project-service'),
  IssueService = require('../service/issue-service'),
  bcrypt = require('bcrypt'),
  jwt = require('jsonwebtoken'),
  config = require('./config');;

class BlueprintRouteHandler extends BaseHelper {
  constructor(dependencies, config) {
    super(dependencies, config);
    this.config = config;
    this.config.appId = repoInfo.name;
  }

  async getBluePrintResponse(request, h) {
    try {
      let service = new BlueprintService(this.dependencies, this.config, request);
      let result = service.getSampleData();

      return this.replySuccess(h, result);
    } catch (error) {
      return this.replyError(h, error);
    }
  }

  async createUser(request, h) {
    try {
        console.log('In route handler');
        let service = new UserService(this.dependencies, this.config, request);
        let result = service.createNewUser();
        return result;
    } catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }

  async createProject(request, h) {
    try {
        console.log('In route handler');
        let service = new ProjectService(this.dependencies, this.config, request);
        let result = service.createProject(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async viewProject(request, h) {
    try {
        console.log('In route handler');
        let service = new ProjectService(this.dependencies, this.config, request);
        let result = service.viewProject(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async createIssue(request, h) {
    try {
        console.log('In route handler');
        let service = new IssueService(this.dependencies, this.config, request);
        let result = service.createIssue(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async viewIssue(request, h) {
    try {
        console.log('In route handler');
        let service = new IssueService(this.dependencies, this.config, request);
        let result = service.viewIssue(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async updateIssue(request, h) {
    try {
        console.log('In route handler');
        let service = new IssueService(this.dependencies, this.config, request);
        let result = service.updateIssue(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async updateProject(request, h) {
    try {
        console.log('In route handler');
        let service = new ProjectService(this.dependencies, this.config, request);
        let result = service.updateProject(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async signup(request, h) {
    try {
        console.log('In route handler');
        let service = new UserService(this.dependencies, this.config, request);
        let result = service.signup(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async workspace(request, h) {
    try {
        console.log('In route handler');
        let service = new UserService(this.dependencies, this.config, request);
        let result = service.workspace(request, h);
        return result;
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async check(request, h) {
    try {
      console.log(request);
        return 'check okay';
    } catch(error) {
      console.log(error);
      return 'Error in Route Handler';
    }
  }

  async validate(request, h) {
    try {
        
      console.log('In route handler');
      let service = new UserService(this.dependencies, this.config, request);
      let result = service.validate(request, h);
      return result;
        
        /*bcrypt.hash('myPassword', 10, function(err, hash) {
            console.log('Pass hash '+hash);
        });

        bcrypt.compare('myPassword','$2b$10$7r0VpIE258HVfHYtWGNfQOfqf5xu.bVPOU2WEkYDxMD4ZoAfDa5Bi', function(err, res) {
            console.log('Innn');
            if(res)
            {
              console.log('Truee');
              return 'Match';
            }
            else
            {
              console.log('Faalse');
              return 'Dont match';
            }
        });
        return 'Pooh';*/
    } catch(error)
    {
      console.log('Error in route handler' + error);
      return error;
    }
  }
}

module.exports = BlueprintRouteHandler;
