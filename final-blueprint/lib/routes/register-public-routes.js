'use strict';

const repoInfo = require('../../repo-info'),
  BlueprintRouteHandler = require('./blueprint-route-handler');


class RegisterPublicRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.blueprintRouteHandler = new BlueprintRouteHandler(dependencies, this.config);
    this.config.appId = repoInfo.name;
  }

  async init(server){
    server.log('Init done for Public Routes')
  }

  registerRoutes(server) {
    server.log('Registering public routes for Blueprint service');

    server.route({
      method: 'POST',
      path: '/v1/project',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.createProject(request, h),
        description: 'Create a new Project',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/project',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.viewProject(request, h),
        description: 'View Project Details',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/v1/issue',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.createIssue(request, h),
        description: 'Create a new Issue',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/issue',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.viewIssue(request, h),
        description: 'View Issue Details',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'PUT',
      path: '/v1/issue',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.updateIssue(request, h),
        description: 'Update an Issue',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'PUT',
      path: '/v1/project',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.updateProject(request, h),
        description: 'Update a Project',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });


    server.route({
      method: 'POST',
      path: '/signup',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.signup(request, h),
        description: 'Registration of User',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/workspace',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.workspace(request, h),
        description: 'Find Workspace of User',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/check',

      config: {
        handler: (request, h) => this.blueprintRouteHandler.check(request, h),
        description: 'Create a new Issue',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/validate',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.validate(request, h),
        description: 'Validates username and password',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.displayParam(request, h),
        description: 'Sample Route',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/hapi',
      handler: {
        file: 'lib/public/index.html'
      }
    });

  }

}

module.exports = RegisterPublicRoutes;
