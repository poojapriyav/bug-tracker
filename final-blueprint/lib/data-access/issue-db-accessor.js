'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const UserSchema = require('../common/schema').UserSchema;

class IssueDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Sample,
      UserSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Sample];
  }

  async get() {
    let values = [];
    try {
      let query = this.sqlCmds.getAll;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

  async createIssue(request, h) {
    let values = [];
    console.log(request);
    try {
      values.push(request.payload.issue_name);
      values.push(request.payload.issue_description);
      values.push(request.payload.issue_severity);
      values.push(request.payload.assigned_to);
      values.push(request.payload.reported_by);
      values.push(request.payload.issue_status);
      values.push(request.payload.project_id);
      values.push(request.payload.workspace);
      let query = this.sqlCmds.createIssue;

      let result = await this.insertOne(query, values);     
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

  async viewIssue(request, h) {

    let values=[];
    let query="";
    try {
      if(request.query.issue_id)
      {
        values.push(request.query.issue_id);
        query = this.sqlCmds.viewIssue;
      }
      else if(request.query.workspace)
      {
        values.push(request.query.workspace);
        query = this.sqlCmds.wsissue;
      }

      else if(request.query.project_id)
      {
        values.push(request.query.project_id);
        query = this.sqlCmds.viewIssuesUnderProject;
      }
      
      else if(request.query.corpid && request.query.action=="reported")
      {
        values.push(request.query.corpid);
        query = this.sqlCmds.viewIssuesReported;
      }

      else if(request.query.corpid && request.query.action=="assigned")
      {
        values.push(request.query.corpid);
        query = this.sqlCmds.viewIssuesAssigned;
      }
      else {
        query = this.sqlCmds.fetchAllIssues;
      }
      console.log(values);
      console.log(query);

      let result = await this.filter(query, values);     
      return result;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

  async updateIssue(request, h) {
    let values = [];
    try {
      values.push(request.payload.issue_id);
      values.push(request.payload.issue_name);
      values.push(request.payload.issue_description);
      values.push(request.payload.issue_severity);
      values.push(request.payload.issue_status);
      values.push(request.payload.assigned_to);
      values.push(request.payload.reported_by);
      values.push(request.payload.workspace);
      console.log(values);
      let query = this.sqlCmds.updateIssue;

      let result = await this.filter(query, values);     
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }
}

module.exports = IssueDbAccessor;
