'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const UserSchema = require('../common/schema').UserSchema;

class UserDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Sample,
      UserSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Sample];
  }


  async validate(request, h) {
    let values = [];
    values.push(request.payload.corpid);
    try {
      let query = this.sqlCmds.validateUser;

      let result = await this.filter(query, values);

      if(result[0].password == request.payload.password)
        return true;
      else
        return false;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'validate', err, { values });
      throw err;
    }
  }

  async signup(request, h) {
    let values = [];
    try {
      values.push(request.payload.corpid);
      values.push(request.payload.password);
      values.push(request.payload.name);
      values.push(request.payload.email);
      values.push(request.payload.contact);
      values.push(request.payload.workspace);
      console.log(values);
      let query = this.sqlCmds.signup;

      let result = await this.insertOne(query, values);     
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

  async workspace(request, h) {
    let values = [];
    
    try {
      values.push(request.query.corpid);
      console.log(values);
      let query = this.sqlCmds.workspace;

      let result = await this.insertOne(query, values);     
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }
  
}

module.exports = UserDbAccessor;
