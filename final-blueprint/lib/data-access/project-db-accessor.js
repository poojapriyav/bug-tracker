'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const UserSchema = require('../common/schema').UserSchema;

class ProjectDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Sample,
      UserSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Sample];
  }

  async createProject(request, h) {
    let values = [];
    
    console.log(values);
    try {
      values.push(request.payload.project_name);
      values.push(request.payload.project_description);
      values.push(request.payload.project_manager);
      values.push(request.payload.project_type);
      values.push(request.payload.workspace);

      console.log(values);

      let query = this.sqlCmds.createProject;
      console.log(query);

      let result = await this.filter(query, values);     
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

  async viewProject(request, h) {
    let values = [];
    let query = "";
    try {
      if(request.query.project_id)
      {
        values.push(request.query.project_id);
        query = this.sqlCmds.viewProject;
      }
      else if(request.query.workspace)
      {
        values.push(request.query.workspace);
        query = this.sqlCmds.wsproj;
      }
      else {
        query = this.sqlCmds.fetchAllProjects;
      }

      console.log(values);

      let result = await this.filter(query, values);   
      
      console.log( 'Proj  return '+ result);
      return result;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }
 

  async updateProject(request, h) {
    let values = [];
  
    try {
      values.push(request.payload.project_id);
      values.push(request.payload.project_name);
      values.push(request.payload.project_description);
      values.push(request.payload.project_type);
      values.push(request.payload.project_manager);
      values.push(request.payload.workspace);
      console.log(values);
      let query = this.sqlCmds.updateProject;

      let result = await this.filter(query, values);     
      return result;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }

}

module.exports = ProjectDbAccessor;
