const Joi = require('joi');

const UserSchema = Joi.object({
  value: Joi.string().description('The actual key').required(),
  desc: Joi.string().description('A short description of this key and its usage').required()
}).unknown(true);

module.exports = {
  UserSchema
}
