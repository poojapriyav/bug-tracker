"use strict";

const { BaseHelper } = require('base-lib');
const ProjectDbAccessor = require('../data-access/project-db-accessor');
const _ = require('lodash');

class ProjectService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.projectDbAccessor = new ProjectDbAccessor(dependencies, config, requestContext);
      }






  async createProject(request, h) {
    try {
        console.log('In user service');
        
        let result = await this.projectDbAccessor.createProject(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }

  async viewProject(request, h) {
    try {
        console.log('In user service');
        
        let result = await this.projectDbAccessor.viewProject(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }

  async updateProject(request, h) {
    try {
        let result = await this.projectDbAccessor.updateProject(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }
       

}

module.exports = ProjectService;
