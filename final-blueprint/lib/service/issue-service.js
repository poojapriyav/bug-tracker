"use strict";

const { BaseHelper } = require('base-lib');
const UserDbAccessor = require('../data-access/user-db-accessor');
const IssueDbAccessor = require('../data-access/issue-db-accessor');
const _ = require('lodash');

class IssueService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.userDbAccessor = new UserDbAccessor(dependencies, config, requestContext);
        this.issueDbAccessor = new IssueDbAccessor(dependencies, config, requestContext);
      }

  async createIssue(request, h) {
    try {
        let result = await this.issueDbAccessor.createIssue(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }

  async viewIssue(request, h) {
    try {
        console.log('In user service');
        
        let result = await this.issueDbAccessor.viewIssue(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }

  async updateIssue(request, h) {
    try {
        let result = await this.issueDbAccessor.updateIssue(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }
}

module.exports = IssueService;
