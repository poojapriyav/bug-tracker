"use strict";

const { BaseHelper } = require('base-lib');
const UserDbAccessor = require('../data-access/user-db-accessor');
const _ = require('lodash');

class UserService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.userDbAccessor = new UserDbAccessor(dependencies, config, requestContext);
      }

  async createNewUser() {
    try {
        console.log('In user service');
        
        let result = await this.userDbAccessor.get();
        console.log(result)
        return result;
       
    } catch (err) {
        return 'Error in user-service';
    }
  }

  async signup(request, h) {
    try {
        let result = await this.userDbAccessor.signup(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }

  async workspace(request, h) {
    try {
        let result = await this.userDbAccessor.workspace(request, h);
        console.log(result)
        return result;
       
    } catch (err) {
        console.log(err);
        return err;
    }
  }


  async validate(request, h) {
    try {
        console.log('In user service');
        
        let result = await this.userDbAccessor.validate(request, h);
        console.log('service result '+result)
        return result;
       
    } catch (err) {
        return err;
    }
  }
}

module.exports = UserService;
