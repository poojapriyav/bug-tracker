'use strict'

const Constants = require('../common/constants');
const load = require('./load-sql');

let sqlFileHash = {};

sqlFileHash[Constants.Tables.Sample] = {

  createIssue: load('./sample/create-issue.sql'),
  viewIssuesAssigned: load('./sample/view-issues-assigned.sql'),
  viewIssuesReported: load('./sample/view-issues-reported.sql'),
  validateUser: load('./sample/validate.sql'),
  getAll: load('./sample/select-all.sql'),
  createProject: load('./sample/create-project.sql'),
  viewProject: load('./sample/view-project.sql'),
  viewIssue: load('./sample/view-issue.sql'),
  updateIssue: load('./sample/update-issue.sql'),
  updateProject: load('./sample/update-project.sql'),
  viewIssuesUnderProject: load('./sample/issues-under-project.sql'),
  signup: load('./sample/signup.sql'),
  workspace: load('./sample/workspace.sql'),
  wsproj: load('./sample/wsproj.sql'),
  wsissue: load('./sample/wsissue.sql'),
  createNotification: load('./sample/create-notification.sql'),
  fetchAllProjects: load('./sample/fetch-all-projects.sql'),
  fetchAllIssues: load('./sample/fetch-all-issues.sql')

}

module.exports = sqlFileHash;
